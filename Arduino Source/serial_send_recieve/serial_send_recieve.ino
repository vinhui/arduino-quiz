const short amountButtons = 8;

// what pins the buttons are in
// the length has to match the button count
const short PinButton[] = { A0, A1, A2, A3, A4, A5, 12, 13 };    // *

// what are the pins the leds are in
// the length has to match the button count
const short PinLed[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

// what character corresponds with what led (led 0 -> 0, led 1 -> 1 etc) *
// this is for receiving bytes
const char LedByte[] = { '0', '1', '2', '3', '4', '5', '6', '7' };

// the pins that the rgp strip is connected to (Red = 1st, Green = 2nd, Blue = 3rd)
const short PinRGBStrip[3] = { 9, 10, 11 };

//Pin connected to latch pin (ST_CP) of 74HC595
const short latchPin = 2;
////Pin connected to Data in (DS) of 74HC595
const short dataPin = 3;
//Pin connected to clock pin (SH_CP) of 74HC595
const short clockPin = 4;

// set the standalone mode, whether it needs a computer to operate
const short PinStandalone = 8;

// after how many miliseconds can someone press the button again
const short buttonPressDelay = 2000;

const short ledBlinkDelay = 500;
const short ledBlinkTimes = 5;

// this array sould be the size of amountButtons 
// these are the colors for when the system is in standalone mode
const short ButtonColors[8][3] = {
	{ 255, 0, 0 },
	{ 0, 255, 0 },
	{ 0, 0, 255 },
	{ 255, 255, 0 },
	{ 255, 0, 255 },
	{ 0, 255, 255 },
	{ 255, 255, 255 },
	{ 0, 127, 0 }
};

// this is the first symbol that is defining the message is a color (when receiving a message over com)
const char colorSign = '#';
// this is the character that determines that the next bytes are for leds (when receiving a message over com)
const char ledSign = '@';

// dont fiddle with these
boolean appendreceive = false;
String receiveMessage;
boolean buttonPressed[] = { false, false, false, false };    // *
long previousMillisButton[] = { 0, 0, 0, 0, 0, 0, 0, 0 };       // *
long previousMillisLed[] = { 0, 0, 0, 0, 0, 0, 0, 0 };          // *
boolean nextIsLed = false;
int ledNum = -1;
boolean nextIsLedState = false;
boolean standalone = true;

////////////////////////////////////////////////////////////////////
// * = array has to be the same length as amount of buttons
////////////////////////////////////////////////////////////////////


// the setup routine runs once when you press reset:
void setup()
{
	Serial.begin(57600);

	//Serial.println("/Starting Arduino");

	// loop #buttonAmount times to set some stuff
	for (int i = 0; i < amountButtons; i++)
	{
		// set all the leds as output
		//pinMode(PinLed[i], OUTPUT);    // removed since we use a shifter now
		// set all the buttons as input
		pinMode(PinButton[i], INPUT);
		// set all the button state's to high
		digitalWrite(PinButton[i], HIGH);
	}

	pinMode(PinStandalone, INPUT);
	digitalWrite(PinStandalone, HIGH);

	pinMode(latchPin, OUTPUT);
	pinMode(dataPin, OUTPUT);
	pinMode(clockPin, OUTPUT);

	if (true)    // just so we are able so skip the startup test sequence
	{
		// turn all the lights on at the start, this way we can check if all the lights are connected properly
		for (int n = 0; n < amountButtons; n++)
		{
			SetLedState(PinLed[n], true);
			delay(1000);
			SetLedState(PinLed[n], false);
			delay(100);
		}

		// and now show some colors on the LED strip
		for (int m = 0; m < 5; m++)
		{
			switch (m)
			{
			case 0:
				SetLedStripColor(255, 255, 255);
				break;
			case 1:
				SetLedStripColor(255, 0, 0);
				break;
			case 2:
				SetLedStripColor(0, 255, 0);
				break;
			case 3:
				SetLedStripColor(0, 0, 255);
				break;
			case 4:
				SetLedStripColor(0, 0, 0);
				break;
			}

			delay(1000);
		}

		Serial.println("/Test sequence done");
	}
}

// the loop routine runs over and over again forever:
void loop()
{
	// Loop through buttons to see if they are pressed
	for (int i = 0; i < amountButtons; i++)
	{
		// if button pressed
		if (digitalRead(PinButton[i]) == LOW && buttonPressed[i] == false)
		{
			// send the button number over serial
			if (standalone)
				FlashLights(PinLed[i], 500, 500, 5, i);
			else
				Serial.println(i);

			buttonPressed[i] = true;

			// set the time for the button press delay
			previousMillisButton[i] = millis();
		}
	}

	if (!standalone)
	{
		// check if there is a message to receive
		if (Serial.available() > 0)
		{
			char incomingByte = (char)Serial.read();

			// check if the character is the colorSign OR if we want to collect the characters
			if (incomingByte == colorSign || appendreceive)
			{
				if (incomingByte == colorSign)
					appendreceive = true;
				else
				{
					if (receiveMessage.length() < 9)
						receiveMessage += incomingByte;

					if (receiveMessage.length() >= 9)
					{
						SetLedStripColor(receiveMessage.substring(0, 3).toInt(), receiveMessage.substring(3, 6).toInt(), receiveMessage.substring(6, 9).toInt());
						receiveMessage = "";
						appendreceive = false;
					}
				}
			}

			if (incomingByte == ledSign || nextIsLed || nextIsLedState)
			{
				if (incomingByte == ledSign)
				{
					nextIsLed = true;
				}

				if (nextIsLed)
				{
					if (nextIsLedState)
					{
						SetLedState(ledNum, (incomingByte == '+' ? true : false));
						nextIsLed = false;
						nextIsLedState = false;
					}

					if (!nextIsLedState)
					{
						// loop # amount of times to check if there is a led linked to the received byte
						for (int n = 0; n < amountButtons; n++)
						{
							// check the byte to the current led
							if (incomingByte == LedByte[n])
							{
								ledNum = n;
								nextIsLedState = true;
							}
						}
					}
				}
			}
		}
	}
	// BUTTONS STATE SET
	for (int a = 0; a < amountButtons; a++)
	{
		// check if the current button is pressed
		if (buttonPressed[a] == true)
		{
			unsigned long currentMillis = millis();

			// check if enough time has passed to set the buttonpressed to false
			if (currentMillis - previousMillisButton[a] > buttonPressDelay)
			{
				buttonPressed[a] = false;
				previousMillisButton[a] = 0;
			}
		}
	}

	if (digitalRead(PinStandalone) == HIGH)
	{
		if (standalone)
		{
			standalone = false;
			//Serial.println("/Standalone false " + String(digitalRead(PinStandalone)) + ";");
		}
	}

	if (digitalRead(PinStandalone) == LOW)
	{
		if (!standalone)
		{
			standalone = true;
			//Serial.println("/Standalone true " + String(digitalRead(PinStandalone)) + ";");
		}
	}
}

void FlashLights(short led, short onTime, short offTime, short amount, short buttonId)
{
	//Serial.print("/Flashing LED ");
	//Serial.print(led);
	//Serial.println();

	for (int i = 0; i < amount; i++)
	{
		SetLedState(led, true);
		SetLedStripColor(ButtonColors[buttonId][0], ButtonColors[buttonId][1], ButtonColors[buttonId][2]);
		delay(onTime);
		SetLedState(led, false);
		SetLedStripColor(ButtonColors[buttonId][0], ButtonColors[buttonId][1], ButtonColors[buttonId][2]);
		delay(offTime);
	}
}

void SetLedState(int led, boolean on)
{
	//Serial.print("/Setting LED ");
	//Serial.print(led);
	//Serial.print(" to ");
	//Serial.print(on);
	//Serial.println();

	if (on)
	{
		//digitalWrite(PinLed[led], HIGH);    // Removed since we use a shifter now
		registerWrite(PinLed[led], HIGH);
	}
	else
	{
		//digitalWrite(PinLed[led], LOW);    // Removed since we use a shifter now
		registerWrite(PinLed[led], LOW);
	}
}

void SetLedStripColor(int red, int green, int blue)
{
	//Serial.print("/Setting LED strip to ");
	//Serial.print(red);
	//Serial.print(", ");
	//Serial.print(green);
	//Serial.print(", ");
	//Serial.print(blue);

	analogWrite(PinRGBStrip[0], red);
	analogWrite(PinRGBStrip[1], green);
	analogWrite(PinRGBStrip[2], blue);
}



// This method sends bits to the shift register:     (Origional arduino code)
void registerWrite(int whichPin, int whichState)
{
	// the bits you want to send
	byte bitsToSend = 0;

	// turn off the output so the pins don't light up
	// while you're shifting bits:
	digitalWrite(latchPin, LOW);

	// turn on the next highest bit in bitsToSend:
	bitWrite(bitsToSend, whichPin, whichState);

	// shift the bits out:
	shiftOut(dataPin, clockPin, MSBFIRST, bitsToSend);

	// turn on the output so the LEDs can light up:
	digitalWrite(latchPin, HIGH);
}
