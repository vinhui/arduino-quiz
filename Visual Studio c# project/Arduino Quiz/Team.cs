﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Arduino_Quiz
{
    class Team
    {
        public string Name;
        public int Score = 0;
        public string Sound;
        public int Button;
        public int Light;
        public string HexColor;
    }
}
