﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace Arduino_Quiz
{
    public partial class TeamForm : Form
    {
        public string TeamNameInput { get { return teamNameInput.Text; } set { teamNameInput.Text = value; } }
        public string TeamSoundInput { get { return teamSoundInput.Text; } set { teamSoundInput.Text = value; } }
        public decimal TeamButtonInput { get { return teamButtonInput.Value; } set { teamButtonInput.Value = value; } }
        public decimal TeamLightInput { get { return teamLightInput.Value; } set { teamLightInput.Value = value; } }
        public decimal TeamScoreInput { get { return teamScoreInput.Value; } set { teamScoreInput.Value = value; } }
        public string TeamColorInput { get { return teamColorInput.Text; } set { teamColorInput.Text = value; } }
        public string TeamSubmitButton { set { TeamSubmit.Text = value; } }
        public string TeamFormName { set { this.Text = value; } }

        public string OldTeamName;

        public TeamForm()
        {
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();

            this.teamSoundInput.DataSource = Program.Sounds.Keys.ToList();
        }

        private void TeamSubmit_Click(object sender, EventArgs e)
        {
            if (TeamSubmit.Text == "Add")
            {
                if (teamNameInput.Text.Trim().Length == 0)
                {
                    MessageBox.Show("You can't leave the team name empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (teamColorInput.Text.Replace(" ", "").Length != 7)
                {
                    MessageBox.Show("You haven't filled in the team color correctly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Team newTeam = new Team();
                newTeam.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(teamNameInput.Text);
                newTeam.Sound = teamSoundInput.Text;
                newTeam.Button = Int16.Parse(teamButtonInput.Text);
                newTeam.Light = Int16.Parse(teamLightInput.Text);
                newTeam.Score = Int16.Parse(teamScoreInput.Text);
                newTeam.HexColor = teamColorInput.Text;

                foreach (Team t in Program.Teams)
                {
                    if (t.Name == newTeam.Name)
                    {
                        MessageBox.Show("This team name is already in use.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                Program.Teams.Add(newTeam);

                this.Close();
            }

            if(TeamSubmit.Text == "Edit")
            {
                foreach (Team t in Program.Teams)
                {
                    if (t.Name == OldTeamName)
                    {
                        t.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(teamNameInput.Text);
                        t.Sound = teamSoundInput.Text;
                        t.Button = Int16.Parse(teamButtonInput.Text);
                        t.Light = Int16.Parse(teamLightInput.Text);
                        t.Score = Int16.Parse(teamScoreInput.Text);
                        t.HexColor = teamColorInput.Text;

                        this.Close();
                        break;
                    }
                }
            }
        }

        private void TeamCancelAdd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ColorPickButton_Click(object sender, EventArgs e)
        {
            DialogResult picker = ColorPicker.ShowDialog();

            if(picker == DialogResult.OK)
            {
                teamColorInput.Text = "#" + ColorPicker.Color.ToArgb().ToString("X8").Substring(2, 6);
            }
        }
    }
}
