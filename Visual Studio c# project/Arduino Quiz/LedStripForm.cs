﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Arduino_Quiz
{
    public partial class LedStripForm : Form
    {
        public LedStripForm()
        {
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();
        }

        private void ColorPickButton_Click(object sender, EventArgs e)
        {
            DialogResult picker = ColorPicker.ShowDialog();

            if (picker == DialogResult.OK)
            {
                Program.LEDstripColor = System.Drawing.ColorTranslator.ToHtml(ColorPicker.Color);
                Program.SetStripColor(ColorPicker.Color);
            }
        }

        private void CloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
