﻿namespace Arduino_Quiz
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SendByte = new System.Windows.Forms.Button();
            this.ByteInput = new System.Windows.Forms.TextBox();
            this.StartSerial = new System.Windows.Forms.Button();
            this.StopSerial = new System.Windows.Forms.Button();
            this.ShowLights = new System.Windows.Forms.CheckBox();
            this.ComPort = new System.Windows.Forms.ComboBox();
            this.PlaySound = new System.Windows.Forms.CheckBox();
            this.TeamsList = new System.Windows.Forms.ListBox();
            this.AddTeam = new System.Windows.Forms.Button();
            this.EditTeam = new System.Windows.Forms.Button();
            this.DelTeam = new System.Windows.Forms.Button();
            this.AddScoreButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.FullscreenWindow = new System.Windows.Forms.Button();
            this.AddScoreInput = new System.Windows.Forms.NumericUpDown();
            this.ResetScores = new System.Windows.Forms.Button();
            this.MoveItemDown = new System.Windows.Forms.Button();
            this.MoveItemUp = new System.Windows.Forms.Button();
            this.OpenSoundboard = new System.Windows.Forms.Button();
            this.OpenTimer = new System.Windows.Forms.Button();
            this.OpenLedStripForm = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.AutomaticTimer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.AddScoreInput)).BeginInit();
            this.SuspendLayout();
            // 
            // SendByte
            // 
            this.SendByte.Location = new System.Drawing.Point(247, 289);
            this.SendByte.Name = "SendByte";
            this.SendByte.Size = new System.Drawing.Size(82, 23);
            this.SendByte.TabIndex = 19;
            this.SendByte.Text = "Send Byte";
            this.SendByte.UseVisualStyleBackColor = true;
            this.SendByte.Click += new System.EventHandler(this.SendByte_Click);
            // 
            // ByteInput
            // 
            this.ByteInput.Location = new System.Drawing.Point(247, 263);
            this.ByteInput.MaxLength = 20;
            this.ByteInput.Name = "ByteInput";
            this.ByteInput.Size = new System.Drawing.Size(82, 20);
            this.ByteInput.TabIndex = 18;
            this.toolTip1.SetToolTip(this.ByteInput, "Use @1+ for enabling LED\'s. Use #255000255 to set the led strip.");
            // 
            // StartSerial
            // 
            this.StartSerial.Location = new System.Drawing.Point(93, 12);
            this.StartSerial.Name = "StartSerial";
            this.StartSerial.Size = new System.Drawing.Size(38, 23);
            this.StartSerial.TabIndex = 2;
            this.StartSerial.Text = "Start";
            this.StartSerial.UseVisualStyleBackColor = true;
            this.StartSerial.Click += new System.EventHandler(this.StartSerial_Click);
            // 
            // StopSerial
            // 
            this.StopSerial.Location = new System.Drawing.Point(137, 12);
            this.StopSerial.Name = "StopSerial";
            this.StopSerial.Size = new System.Drawing.Size(38, 23);
            this.StopSerial.TabIndex = 3;
            this.StopSerial.Text = "Stop";
            this.StopSerial.UseVisualStyleBackColor = true;
            this.StopSerial.Click += new System.EventHandler(this.StopSerial_Click);
            // 
            // ShowLights
            // 
            this.ShowLights.AutoSize = true;
            this.ShowLights.Checked = true;
            this.ShowLights.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLights.Location = new System.Drawing.Point(228, 12);
            this.ShowLights.Name = "ShowLights";
            this.ShowLights.Size = new System.Drawing.Size(83, 17);
            this.ShowLights.TabIndex = 13;
            this.ShowLights.Text = "Show Lights";
            this.ShowLights.UseVisualStyleBackColor = true;
            this.ShowLights.CheckedChanged += new System.EventHandler(this.ShowLights_CheckedChanged);
            // 
            // ComPort
            // 
            this.ComPort.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComPort.FormattingEnabled = true;
            this.ComPort.Location = new System.Drawing.Point(12, 12);
            this.ComPort.Name = "ComPort";
            this.ComPort.Size = new System.Drawing.Size(75, 21);
            this.ComPort.TabIndex = 1;
            // 
            // PlaySound
            // 
            this.PlaySound.AutoSize = true;
            this.PlaySound.Checked = true;
            this.PlaySound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PlaySound.Location = new System.Drawing.Point(228, 35);
            this.PlaySound.Name = "PlaySound";
            this.PlaySound.Size = new System.Drawing.Size(84, 17);
            this.PlaySound.TabIndex = 14;
            this.PlaySound.Text = "Play Sounds";
            this.PlaySound.UseVisualStyleBackColor = true;
            this.PlaySound.CheckedChanged += new System.EventHandler(this.PlaySound_CheckedChanged);
            // 
            // TeamsList
            // 
            this.TeamsList.FormattingEnabled = true;
            this.TeamsList.Location = new System.Drawing.Point(12, 182);
            this.TeamsList.Name = "TeamsList";
            this.TeamsList.Size = new System.Drawing.Size(120, 56);
            this.TeamsList.TabIndex = 5;
            // 
            // AddTeam
            // 
            this.AddTeam.Location = new System.Drawing.Point(12, 244);
            this.AddTeam.Name = "AddTeam";
            this.AddTeam.Size = new System.Drawing.Size(36, 23);
            this.AddTeam.TabIndex = 8;
            this.AddTeam.Text = "Add";
            this.AddTeam.UseVisualStyleBackColor = true;
            this.AddTeam.Click += new System.EventHandler(this.AddTeam_Click);
            // 
            // EditTeam
            // 
            this.EditTeam.Location = new System.Drawing.Point(54, 244);
            this.EditTeam.Name = "EditTeam";
            this.EditTeam.Size = new System.Drawing.Size(35, 23);
            this.EditTeam.TabIndex = 9;
            this.EditTeam.Text = "Edit";
            this.EditTeam.UseVisualStyleBackColor = true;
            this.EditTeam.Click += new System.EventHandler(this.EditTeam_Click);
            // 
            // DelTeam
            // 
            this.DelTeam.Location = new System.Drawing.Point(95, 244);
            this.DelTeam.Name = "DelTeam";
            this.DelTeam.Size = new System.Drawing.Size(37, 23);
            this.DelTeam.TabIndex = 10;
            this.DelTeam.Text = "Del";
            this.DelTeam.UseVisualStyleBackColor = true;
            this.DelTeam.Click += new System.EventHandler(this.DelTeam_Click);
            // 
            // AddScoreButton
            // 
            this.AddScoreButton.Location = new System.Drawing.Point(95, 289);
            this.AddScoreButton.Name = "AddScoreButton";
            this.AddScoreButton.Size = new System.Drawing.Size(37, 23);
            this.AddScoreButton.TabIndex = 12;
            this.AddScoreButton.Text = "Add";
            this.AddScoreButton.UseVisualStyleBackColor = true;
            this.AddScoreButton.Click += new System.EventHandler(this.AddScoreButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 294);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Score:";
            // 
            // FullscreenWindow
            // 
            this.FullscreenWindow.Location = new System.Drawing.Point(12, 102);
            this.FullscreenWindow.Name = "FullscreenWindow";
            this.FullscreenWindow.Size = new System.Drawing.Size(119, 23);
            this.FullscreenWindow.TabIndex = 4;
            this.FullscreenWindow.Text = "Toggle FullScreen";
            this.FullscreenWindow.UseVisualStyleBackColor = true;
            this.FullscreenWindow.Click += new System.EventHandler(this.FullscreenWindow_Click);
            // 
            // AddScoreInput
            // 
            this.AddScoreInput.Location = new System.Drawing.Point(54, 292);
            this.AddScoreInput.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.AddScoreInput.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.AddScoreInput.Name = "AddScoreInput";
            this.AddScoreInput.Size = new System.Drawing.Size(35, 20);
            this.AddScoreInput.TabIndex = 11;
            // 
            // ResetScores
            // 
            this.ResetScores.Location = new System.Drawing.Point(247, 207);
            this.ResetScores.Name = "ResetScores";
            this.ResetScores.Size = new System.Drawing.Size(82, 23);
            this.ResetScores.TabIndex = 17;
            this.ResetScores.Text = "Reset Teams";
            this.ResetScores.UseVisualStyleBackColor = true;
            this.ResetScores.Click += new System.EventHandler(this.ResetTeams_Click);
            // 
            // MoveItemDown
            // 
            this.MoveItemDown.Location = new System.Drawing.Point(138, 215);
            this.MoveItemDown.Name = "MoveItemDown";
            this.MoveItemDown.Size = new System.Drawing.Size(24, 23);
            this.MoveItemDown.TabIndex = 7;
            this.MoveItemDown.Text = "↓ ";
            this.MoveItemDown.UseVisualStyleBackColor = true;
            this.MoveItemDown.Click += new System.EventHandler(this.MoveItemDown_Click);
            // 
            // MoveItemUp
            // 
            this.MoveItemUp.Location = new System.Drawing.Point(139, 182);
            this.MoveItemUp.Name = "MoveItemUp";
            this.MoveItemUp.Size = new System.Drawing.Size(23, 23);
            this.MoveItemUp.TabIndex = 6;
            this.MoveItemUp.Text = "↑ ";
            this.MoveItemUp.UseVisualStyleBackColor = true;
            this.MoveItemUp.Click += new System.EventHandler(this.MoveItemUp_Click);
            // 
            // OpenSoundboard
            // 
            this.OpenSoundboard.Location = new System.Drawing.Point(247, 112);
            this.OpenSoundboard.Name = "OpenSoundboard";
            this.OpenSoundboard.Size = new System.Drawing.Size(82, 23);
            this.OpenSoundboard.TabIndex = 15;
            this.OpenSoundboard.Text = "Soundboard";
            this.OpenSoundboard.UseVisualStyleBackColor = true;
            this.OpenSoundboard.Click += new System.EventHandler(this.OpenSoundboard_Click);
            // 
            // OpenTimer
            // 
            this.OpenTimer.Location = new System.Drawing.Point(247, 142);
            this.OpenTimer.Name = "OpenTimer";
            this.OpenTimer.Size = new System.Drawing.Size(82, 23);
            this.OpenTimer.TabIndex = 16;
            this.OpenTimer.Text = "Timer";
            this.OpenTimer.UseVisualStyleBackColor = true;
            this.OpenTimer.Click += new System.EventHandler(this.OpenTimer_Click);
            // 
            // OpenLedStripForm
            // 
            this.OpenLedStripForm.Location = new System.Drawing.Point(247, 172);
            this.OpenLedStripForm.Name = "OpenLedStripForm";
            this.OpenLedStripForm.Size = new System.Drawing.Size(82, 23);
            this.OpenLedStripForm.TabIndex = 20;
            this.OpenLedStripForm.Text = "Led Strip";
            this.OpenLedStripForm.UseVisualStyleBackColor = true;
            this.OpenLedStripForm.Click += new System.EventHandler(this.OpenLedStripForm_Click);
            // 
            // AutomaticTimer
            // 
            this.AutomaticTimer.Location = new System.Drawing.Point(228, 58);
            this.AutomaticTimer.Name = "AutomaticTimer";
            this.AutomaticTimer.Size = new System.Drawing.Size(101, 23);
            this.AutomaticTimer.TabIndex = 22;
            this.AutomaticTimer.Text = "Automatic Timer";
            this.AutomaticTimer.UseVisualStyleBackColor = true;
            this.AutomaticTimer.Click += new System.EventHandler(this.AutomaticTimer_Click);
            // 
            // MainWindow
            // 
            this.AcceptButton = this.SendByte;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 323);
            this.Controls.Add(this.AutomaticTimer);
            this.Controls.Add(this.OpenLedStripForm);
            this.Controls.Add(this.OpenTimer);
            this.Controls.Add(this.OpenSoundboard);
            this.Controls.Add(this.MoveItemUp);
            this.Controls.Add(this.MoveItemDown);
            this.Controls.Add(this.ResetScores);
            this.Controls.Add(this.AddScoreInput);
            this.Controls.Add(this.FullscreenWindow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddScoreButton);
            this.Controls.Add(this.DelTeam);
            this.Controls.Add(this.EditTeam);
            this.Controls.Add(this.AddTeam);
            this.Controls.Add(this.TeamsList);
            this.Controls.Add(this.PlaySound);
            this.Controls.Add(this.ComPort);
            this.Controls.Add(this.ShowLights);
            this.Controls.Add(this.StopSerial);
            this.Controls.Add(this.StartSerial);
            this.Controls.Add(this.ByteInput);
            this.Controls.Add(this.SendByte);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Quiz Control";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Settings_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.AddScoreInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SendByte;
        private System.Windows.Forms.TextBox ByteInput;
        private System.Windows.Forms.Button StartSerial;
        private System.Windows.Forms.Button StopSerial;
        private System.Windows.Forms.CheckBox ShowLights;
        private System.Windows.Forms.ComboBox ComPort;
        private System.Windows.Forms.CheckBox PlaySound;
        private System.Windows.Forms.ListBox TeamsList;
        private System.Windows.Forms.Button AddTeam;
        private System.Windows.Forms.Button EditTeam;
        private System.Windows.Forms.Button DelTeam;
        private System.Windows.Forms.Button AddScoreButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button FullscreenWindow;
        private System.Windows.Forms.NumericUpDown AddScoreInput;
        private System.Windows.Forms.Button ResetScores;
        private System.Windows.Forms.Button MoveItemDown;
        private System.Windows.Forms.Button MoveItemUp;
        private System.Windows.Forms.Button OpenSoundboard;
        private System.Windows.Forms.Button OpenTimer;
        private System.Windows.Forms.Button OpenLedStripForm;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button AutomaticTimer;
    }
}

