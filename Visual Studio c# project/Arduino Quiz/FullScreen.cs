﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Arduino_Quiz
{
    public partial class FullScreen : Form
    {
        private Font f_scoreText = DefaultFont;
        private Font f_teamText = DefaultFont;
        private SolidBrush b_scoreText = new SolidBrush(Color.White);
        private SolidBrush b_teamText = new SolidBrush(Color.White);

        private Dictionary<string, Label> teamScoreLabel = new Dictionary<string, Label>();

        private int maxBoxesInWidth = Int16.Parse(Program.settingsIni.IniReadValue("Fullscreen", "MaxColls"));
        private int screenMargin = Int16.Parse(Program.settingsIni.IniReadValue("Fullscreen", "Margin"));
        private string titleFontColor = Program.settingsIni.IniReadValue("Fullscreen", "TitleFontColor");
        private string scoreFontColor = Program.settingsIni.IniReadValue("Fullscreen", "ScoreFontColor");

        private string titleFont = Program.settingsIni.IniReadValue("Fullscreen", "TitleFont");
        private string scoreFont = Program.settingsIni.IniReadValue("Fullscreen", "ScoreFont");

        public FullScreen()
        {
            this.BackgroundImage = Image.FromFile(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Fullscreen", "Background")));
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();
            this.Paint += FullScreen_Paint;
        }

        void FullScreen_Paint(object sender, PaintEventArgs e)
        {
            if (Program.Teams.Count < 1)
                return;

            int screenWidth = (Program.GetSecondaryScreen() == null ? Screen.PrimaryScreen.Bounds.Width : Program.GetSecondaryScreen().Bounds.Width);
            int screenHeight = (Program.GetSecondaryScreen() == null ? Screen.PrimaryScreen.Bounds.Height : Program.GetSecondaryScreen().Bounds.Height);

            int curRow = 0;
            int curCol = 0;

            int maxCols = maxBoxesInWidth;

            if (Program.Teams.Count < maxBoxesInWidth)
            {
                maxCols = Program.Teams.Count;
            }

            int rows = (int)Math.Ceiling((double)Program.Teams.Count / maxCols);

            int boxWidth = (screenWidth - screenMargin * 2) / maxCols;
            int boxHeight = (screenHeight - screenMargin * 2) / rows;

            Color backgroundColor = Color.FromArgb(128, 100, 100, 100);
            backgroundColor = Color.Transparent;

            this.f_scoreText = new Font(this.scoreFont, boxHeight / 2);
            this.f_teamText = new Font(this.titleFont, 100 / rows);

            this.b_scoreText = new SolidBrush(ColorTranslator.FromHtml(this.scoreFontColor));
            this.b_teamText = new SolidBrush(ColorTranslator.FromHtml(this.titleFontColor));

            for (int i = 0; i < Program.Teams.Count; i++)
            {
                Team curTeam = Program.Teams[i];

                if (i % maxCols == 0)
                {
                    curRow++;
                    curCol = 0;
                }

                Point boxPos = new Point(screenMargin + boxWidth * curCol, screenMargin + boxHeight * (curRow - 1));
                e.Graphics.FillRectangle(new SolidBrush(backgroundColor), new Rectangle(boxPos, new Size(boxWidth, boxHeight)));

                StringFormat sfScore = new StringFormat();
                sfScore.Alignment = StringAlignment.Center;
                sfScore.LineAlignment = StringAlignment.Center;

                Point scorePos = new Point(boxPos.X + boxWidth / 2, boxPos.Y + boxHeight / 2 + boxHeight / 8);
                e.Graphics.DrawString(curTeam.Score.ToString(), this.f_scoreText, this.b_scoreText, scorePos, sfScore);

                StringFormat sfTeam = new StringFormat();
                sfTeam.Alignment = StringAlignment.Center;
                sfTeam.LineAlignment = StringAlignment.Near;

                Point teamPos = new Point(boxPos.X + boxWidth / 2, boxPos.Y);
                e.Graphics.DrawString(curTeam.Name, this.f_teamText, this.b_teamText, teamPos, sfTeam);

                curCol++;
            }

            e.Dispose();
        }

        private void FullScreen_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;

            if (Program.GetSecondaryScreen() != null)
                this.Bounds = Program.GetSecondaryScreen().Bounds;
            else
                this.Bounds = Screen.PrimaryScreen.Bounds;
        }

        public void RefreshScreen()
        {
            this.Refresh();
        }
    }
}