﻿namespace Arduino_Quiz
{
    partial class TimerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TimerStop = new System.Windows.Forms.Button();
            this.TimerStart = new System.Windows.Forms.Button();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.TimerInputMin = new System.Windows.Forms.MaskedTextBox();
            this.TimerInputSec = new System.Windows.Forms.MaskedTextBox();
            this.PlaySound = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TimerStop
            // 
            this.TimerStop.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TimerStop.Location = new System.Drawing.Point(205, 235);
            this.TimerStop.Name = "TimerStop";
            this.TimerStop.Size = new System.Drawing.Size(75, 23);
            this.TimerStop.TabIndex = 5;
            this.TimerStop.Text = "Stop";
            this.TimerStop.UseVisualStyleBackColor = true;
            this.TimerStop.Click += new System.EventHandler(this.TimerStop_Click);
            // 
            // TimerStart
            // 
            this.TimerStart.Location = new System.Drawing.Point(124, 235);
            this.TimerStart.Name = "TimerStart";
            this.TimerStart.Size = new System.Drawing.Size(75, 23);
            this.TimerStart.TabIndex = 4;
            this.TimerStart.Text = "Start";
            this.TimerStart.UseVisualStyleBackColor = true;
            this.TimerStart.Click += new System.EventHandler(this.TimerStart_Click);
            // 
            // TimeLabel
            // 
            this.TimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeLabel.Location = new System.Drawing.Point(-1, -2);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(294, 200);
            this.TimeLabel.TabIndex = 0;
            this.TimeLabel.Text = "00:00";
            this.TimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TimerInputMin
            // 
            this.TimerInputMin.Location = new System.Drawing.Point(17, 237);
            this.TimerInputMin.Mask = "00";
            this.TimerInputMin.Name = "TimerInputMin";
            this.TimerInputMin.Size = new System.Drawing.Size(31, 20);
            this.TimerInputMin.TabIndex = 1;
            this.TimerInputMin.Text = "00";
            // 
            // TimerInputSec
            // 
            this.TimerInputSec.Location = new System.Drawing.Point(54, 237);
            this.TimerInputSec.Mask = "00";
            this.TimerInputSec.Name = "TimerInputSec";
            this.TimerInputSec.Size = new System.Drawing.Size(31, 20);
            this.TimerInputSec.TabIndex = 2;
            this.TimerInputSec.Text = "05";
            // 
            // PlaySound
            // 
            this.PlaySound.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PlaySound.FormattingEnabled = true;
            this.PlaySound.Location = new System.Drawing.Point(171, 208);
            this.PlaySound.Name = "PlaySound";
            this.PlaySound.Size = new System.Drawing.Size(109, 21);
            this.PlaySound.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 211);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Play sound";
            // 
            // TimerForm
            // 
            this.AcceptButton = this.TimerStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TimerStop;
            this.ClientSize = new System.Drawing.Size(292, 270);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PlaySound);
            this.Controls.Add(this.TimerInputSec);
            this.Controls.Add(this.TimerInputMin);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.TimerStart);
            this.Controls.Add(this.TimerStop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "TimerForm";
            this.Text = "Timer";
            this.Load += new System.EventHandler(this.TimerForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button TimerStop;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.MaskedTextBox TimerInputMin;
        private System.Windows.Forms.MaskedTextBox TimerInputSec;
        private System.Windows.Forms.ComboBox PlaySound;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button TimerStart;
    }
}