﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Media;
using System.IO;

namespace Arduino_Quiz
{
    static class Program
    {
        public static SerialPort SerialPort;
        public static string SerialSendData = "";

        public static bool EnableLights;
        public static bool PlaySounds;

        public static FullScreen FullScreenForm;

        public static List<Team> Teams = new List<Team>();
        //public static List<string> Sounds = new List<string>();
        public static Dictionary<string, string> Sounds = new Dictionary<string, string>();

        public static string IniFileName = "settings.ini";
        public static IniFile settingsIni = new IniFile(Path.Combine(Application.StartupPath, IniFileName));

        public static string LEDstripColor = "#000000";

        private static bool PressedButton = false;
        private static int ButtonPressDelay = 5000;

        public static TimerForm AutomaticTimerForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SerialPort = new SerialPort();

            SetDefaultSettings();

            if (Directory.Exists(Path.Combine(Application.StartupPath, settingsIni.IniReadValue("Sounds", "Directory"))))
            {
                string[] files = Directory.GetFiles(Path.Combine(Application.StartupPath, settingsIni.IniReadValue("Sounds", "Directory")), "*.wav", SearchOption.AllDirectories);

                foreach (string f in files)
                {
                    Sounds.Add(Path.GetFileNameWithoutExtension(f), f);
                }

                //Sounds = Directory.GetFiles(Path.Combine(Application.StartupPath, settingsIni.IniReadValue("Sounds", "Directory")), "*.wav", SearchOption.AllDirectories).Select(Path.GetFileNameWithoutExtension).ToList();
            }
            else
                MessageBox.Show("The sounds directory specified in the " + IniFileName + " does not exist!", "Sounds", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (!Directory.Exists(Path.Combine(Application.StartupPath, settingsIni.IniReadValue("Images", "Directory"))))
                MessageBox.Show("The images directory specified in the " + IniFileName + " does not exist!", "Images", MessageBoxButtons.OK, MessageBoxIcon.Error);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }

        private static void SetDefaultSettings()
        {
            if (settingsIni.IniReadValue("Sounds", "Directory").Length < 1)
                settingsIni.IniWriteValue("Sounds", "Directory", "Sounds/");

            if (settingsIni.IniReadValue("Soundboard", "MaxRows").Length < 1)
                settingsIni.IniWriteValue("Soundboard", "MaxRows", "7");

            if (settingsIni.IniReadValue("Soundboard", "Margin").Length < 1)
                settingsIni.IniWriteValue("Soundboard", "Margin", "10");

            if (settingsIni.IniReadValue("Images", "Directory").Length < 1)
                settingsIni.IniWriteValue("Images", "Directory", "Images/");

            if (settingsIni.IniReadValue("Images", "Icon").Length < 1)
                settingsIni.IniWriteValue("Images", "Icon", "icon.ico");

            if (settingsIni.IniReadValue("Fullscreen", "Margin").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "Margin", "100");

            if (settingsIni.IniReadValue("Fullscreen", "TitleFont").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "TitleFont", "Arial");

            if (settingsIni.IniReadValue("Fullscreen", "TitleFontSize").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "TitleFontSize", "96");

            if (settingsIni.IniReadValue("Fullscreen", "TitleFontColor").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "TitleFontColor", "#FFFFFF");

            if (settingsIni.IniReadValue("Fullscreen", "ScoreFont").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "ScoreFont", "Arial");

            if (settingsIni.IniReadValue("Fullscreen", "ScoreFontSize").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "ScoreFontSize", "400");

            if (settingsIni.IniReadValue("Fullscreen", "ScoreFontColor").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "ScoreFontColor", "#FFFFFF");

            if (settingsIni.IniReadValue("Fullscreen", "MaxColls").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "MaxColls", "2");

            if (settingsIni.IniReadValue("Fullscreen", "Background").Length < 1)
                settingsIni.IniWriteValue("Fullscreen", "Background", "bg.png");

        }

        public static void StartSerial(string comPort)
        {
            if (SerialPort.IsOpen)
                return;

            if (comPort == "")
            {
                MessageBox.Show("COM port cannot be empty!", "COM port", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Thread readSerial = new Thread(ReadSerial);
            //Thread sendSerial = new Thread(SendSerial);

            SerialPort.PortName = comPort;
            SerialPort.BaudRate = 57600;

            SerialPort.ReadTimeout = 500;
            SerialPort.WriteTimeout = 500;

            if (!SerialPort.GetPortNames().Contains(comPort.ToUpper()))
            {
                MessageBox.Show("COM port is not accessable!", "COM port", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SerialPort.Open();

            readSerial.Start();
            //sendSerial.Start();
        }

        public static void StopSerial()
        {
            if (!SerialPort.IsOpen)
                return;

            SerialPort.Close();
        }

        public static void ReadSerial()
        {
            Thread.Sleep(1000);

            while (SerialPort.IsOpen)
            {
                try
                {
                    string message = SerialPort.ReadLine();

                    Console.WriteLine(message);

                    Int16 buttonPressed;

                    if (message.Length > 0)
                    {
                        if (message[0] != '/')
                        {
                            if (Int16.TryParse(message, out buttonPressed))
                            {
                                foreach (Team t in Program.Teams)
                                {
                                    if (t.Button == buttonPressed && !PressedButton)
                                    {
                                        Console.WriteLine("Button pressed");

                                        PressedButton = true;
                                        SerialPort.DiscardInBuffer();
                                        ButtonPressed(t);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("There was debug info: ");
                            Console.WriteLine(message.Substring(1));
                        }
                    }
                }
                catch (TimeoutException) { }
                catch (System.IO.IOException) { }

                Thread.Sleep(10);
            }
        }

        private static void ButtonPressed(Team t)
        {
            Console.WriteLine("Button pressed");

            ThreadStart l = () => BlinkLight(t);
            new Thread(l).Start();
            PlaySound(Sounds[t.Sound]);

            new Thread(ResetButtonPressed).Start();

            if (AutomaticTimerForm != null)
            {
                AutomaticTimerForm.TimerStart_Click(null, null);
            }
        }

        private static void ResetButtonPressed()
        {
            Thread.Sleep(ButtonPressDelay);
            PressedButton = false;
        }

        public static void SerialSend(string data)
        {
            if (SerialPort.IsOpen)
            {
                SerialPort.Write(data);
                SerialPort.DiscardOutBuffer();
            }
        }

        /*
        public static void SendSerial()
        {
            Thread.Sleep(1000);

            while (SerialPort.IsOpen)
            {
                while (SerialSendData != "")
                {
                    try
                    {
                        Console.WriteLine(SerialSendData);
                        SerialPort.Write(SerialSendData);
                        SerialPort.DiscardOutBuffer();
                    }
                    catch (System.IO.IOException) { }
                    SerialSendData = "";
                }

                Thread.Sleep(5);
            }
        }*/

        public static void BlinkLight(Team t)
        {
            if (EnableLights)
            {
                ThreadStart l = () => BlinkLed(t.Light);
                new Thread(l).Start();
                Thread.Sleep(10);
                ThreadStart s = () => BlinkStripColor(t.HexColor);
                new Thread(s).Start();
            }
        }

        public static void BlinkLed(int led, int onTime = 500, int offTime = 500, int amount = 5)
        {
            for (int i = 0; i < amount; i++)
            {
                SetLedState(led, true);
                Thread.Sleep(onTime);
                SetLedState(led, false);
                Thread.Sleep(offTime);
            }
        }

        public static void SetLedState(int led, bool state = true)
        {
            //SerialSendData += "@" + led + (state ? "+" : "-");
            //Console.WriteLine("@" + led + (state ? "+" : "-"));
            SerialSend("@" + led + (state ? "+" : "-"));
            //SerialPort.Write("@" + led + (state ? "+" : "-"));
            //SerialPort.DiscardOutBuffer();
        }

        public static void BlinkStripColor(string color, string offColor = "#000000", int onTime = 500, int offTime = 500, int amount = 5)
        {
            System.Drawing.Color c = System.Drawing.ColorTranslator.FromHtml(color);
            System.Drawing.Color offc = System.Drawing.ColorTranslator.FromHtml(offColor);

            BlinkStripColor(c, offc, onTime, offTime, amount);
        }

        public static void BlinkStripColor(System.Drawing.Color color, System.Drawing.Color offColor, int onTime = 500, int offTime = 500, int amount = 5)
        {
            for (int i = 0; i < amount; i++)
            {
                SetStripColor(color);
                Thread.Sleep(onTime);
                SetStripColor(offColor);
                Thread.Sleep(offTime);
            }

            SetStripColor(LEDstripColor);
        }

        public static void SetStripColor(string HexColor)
        {
            System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(HexColor);

            SetStripColor(color);
        }

        public static void SetStripColor(System.Drawing.Color color)
        {
            //Console.WriteLine("Set ledstrip color: " + color);
            //SerialSendData += ConvertColor(color);
            //Console.WriteLine(ConvertColor(color));
            SerialSend(ConvertColor(color));
            //SerialPort.Write(ConvertColor(color));
            //SerialPort.DiscardOutBuffer();
        }

        public static string ConvertColor(System.Drawing.Color color)
        {
            string red = color.R.ToString();
            string green = color.G.ToString();
            string blue = color.B.ToString();

            red = (red.Length < 2 ? "00" + red : (red.Length < 3 ? "0" + red : red));
            green = (green.Length < 2 ? "00" + green : (green.Length < 3 ? "0" + green : green));
            blue = (blue.Length < 2 ? "00" + blue : (blue.Length < 3 ? "0" + blue : blue));

            return "#" + red + green + blue;
        }

        public static void PlaySound(string sound)
        {
            if (!PlaySounds)
                return;

            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = Path.Combine(Application.StartupPath, settingsIni.IniReadValue("sounds", "directory"), sound);
            player.Play();
        }

        public static Screen GetSecondaryScreen()
        {
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen != Screen.PrimaryScreen)
                    return screen;
            }
            return null;
        }
    }
}
