﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace Arduino_Quiz
{
    public partial class SoundboardForm : Form
    {
        private int maxButtonsDown = Int16.Parse(Program.settingsIni.IniReadValue("Soundboard", "MaxRows"));
        private int buttonMargin = Int16.Parse(Program.settingsIni.IniReadValue("Soundboard", "Margin"));

        public SoundboardForm()
        {
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();
        }

        private void Soundboard_Load(object sender, EventArgs e)
        {
            int i = 0;
            int row = 0;
            int col = 0;

            foreach (KeyValuePair<string, string> s in Program.Sounds)
            {
                if (i % maxButtonsDown == 0)
                {
                    col++;
                    row = 0;
                }

                Button b = new Button();
                b.Text = s.Key;
                b.Location = new Point(buttonMargin + (col - 1) * 100, buttonMargin + row * 30);
                b.Click += new System.EventHandler(PlaySound);
                b.AutoSize = true;
                this.Controls.Add(b);

                i++;
                row++;
            }
        }

        private void PlaySound(object sender, EventArgs e)
        {
            Button click = (Button)sender;

            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = Program.Sounds[click.Text];
            player.Play();
        }

        private void CloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
