﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Arduino_Quiz
{
    public partial class TimerForm : Form
    {
        Timer timerSeconds = new Timer();
        int minutes = 0;
        int seconds = 0;

        public TimerForm()
        {
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();
        }

        private void TimerForm_Load(object sender, EventArgs e)
        {
            timerSeconds.Tick += new EventHandler(Timer_Tick_Seconds);
            timerSeconds.Interval = 1000;

            PlaySound.DataSource = Program.Sounds.Keys.ToList();
        }

        public void TimerStart_Click(object sender, EventArgs e)
        {
            minutes = TimerInputMin.Text.Length < 1 ? 0 : Int16.Parse(TimerInputMin.Text);
            seconds = TimerInputSec.Text.Length < 1 ? 0 : Int16.Parse(TimerInputSec.Text);

            SetLabel();

            timerSeconds.Start();
            timerSeconds.Enabled = true;
        }

        private void TimerStop_Click(object sender, EventArgs e)
        {
            StopTimer();
        }

        private void Timer_Tick_Seconds(object sender, EventArgs e)
        {
            if (seconds > 0)
            {
                seconds--;
            }
            else
            {
                if (minutes > 0)
                {
                    seconds = 59;
                    minutes--;
                }
            }

            if (minutes < 1 && seconds < 1)
            {
                StopTimer();
                Program.PlaySound(Program.Sounds[PlaySound.Text]);
                MessageBox.Show("Time's up!");
            }

            SetLabel();
        }

        private void StopTimer()
        {
            timerSeconds.Stop();
            timerSeconds.Enabled = false;

            minutes = 0;
            seconds = 0;
            SetLabel();
        }

        private void SetLabel()
        {
            TimeLabel.Text = (minutes.ToString().Length < 2 ? "0" + minutes.ToString() : minutes.ToString()) + ":" + (seconds.ToString().Length < 2 ? "0" + seconds.ToString() : seconds.ToString());
        }
    }
}
