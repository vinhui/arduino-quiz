﻿namespace Arduino_Quiz
{
    partial class TeamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.teamNameInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TeamSubmit = new System.Windows.Forms.Button();
            this.TeamCancelAdd = new System.Windows.Forms.Button();
            this.teamButtonInput = new System.Windows.Forms.NumericUpDown();
            this.teamSoundInput = new System.Windows.Forms.ComboBox();
            this.teamColorInput = new System.Windows.Forms.MaskedTextBox();
            this.ColorPicker = new System.Windows.Forms.ColorDialog();
            this.ColorPickButton = new System.Windows.Forms.Button();
            this.teamScoreInput = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.teamLightInput = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.teamButtonInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamScoreInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamLightInput)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Team Name";
            // 
            // teamNameInput
            // 
            this.teamNameInput.Location = new System.Drawing.Point(88, 12);
            this.teamNameInput.Name = "teamNameInput";
            this.teamNameInput.Size = new System.Drawing.Size(100, 20);
            this.teamNameInput.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sound";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Button #";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Color";
            // 
            // TeamSubmit
            // 
            this.TeamSubmit.Location = new System.Drawing.Point(26, 175);
            this.TeamSubmit.Name = "TeamSubmit";
            this.TeamSubmit.Size = new System.Drawing.Size(75, 23);
            this.TeamSubmit.TabIndex = 8;
            this.TeamSubmit.Text = "Add";
            this.TeamSubmit.UseVisualStyleBackColor = true;
            this.TeamSubmit.Click += new System.EventHandler(this.TeamSubmit_Click);
            // 
            // TeamCancelAdd
            // 
            this.TeamCancelAdd.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TeamCancelAdd.Location = new System.Drawing.Point(108, 175);
            this.TeamCancelAdd.Name = "TeamCancelAdd";
            this.TeamCancelAdd.Size = new System.Drawing.Size(75, 23);
            this.TeamCancelAdd.TabIndex = 9;
            this.TeamCancelAdd.Text = "Cancel";
            this.TeamCancelAdd.UseVisualStyleBackColor = true;
            this.TeamCancelAdd.Click += new System.EventHandler(this.TeamCancelAdd_Click);
            // 
            // teamButtonInput
            // 
            this.teamButtonInput.Location = new System.Drawing.Point(88, 64);
            this.teamButtonInput.Name = "teamButtonInput";
            this.teamButtonInput.Size = new System.Drawing.Size(100, 20);
            this.teamButtonInput.TabIndex = 3;
            // 
            // teamSoundInput
            // 
            this.teamSoundInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamSoundInput.FormattingEnabled = true;
            this.teamSoundInput.Location = new System.Drawing.Point(88, 38);
            this.teamSoundInput.Name = "teamSoundInput";
            this.teamSoundInput.Size = new System.Drawing.Size(100, 21);
            this.teamSoundInput.TabIndex = 2;
            // 
            // teamColorInput
            // 
            this.teamColorInput.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.teamColorInput.Location = new System.Drawing.Point(88, 142);
            this.teamColorInput.Mask = "\\#>AAAAAA";
            this.teamColorInput.Name = "teamColorInput";
            this.teamColorInput.PromptChar = ' ';
            this.teamColorInput.Size = new System.Drawing.Size(54, 20);
            this.teamColorInput.TabIndex = 6;
            this.teamColorInput.Text = "000000";
            // 
            // ColorPicker
            // 
            this.ColorPicker.AnyColor = true;
            this.ColorPicker.Color = System.Drawing.Color.Red;
            this.ColorPicker.FullOpen = true;
            // 
            // ColorPickButton
            // 
            this.ColorPickButton.Location = new System.Drawing.Point(148, 142);
            this.ColorPickButton.Name = "ColorPickButton";
            this.ColorPickButton.Size = new System.Drawing.Size(40, 20);
            this.ColorPickButton.TabIndex = 7;
            this.ColorPickButton.Text = "Pick";
            this.ColorPickButton.UseVisualStyleBackColor = true;
            this.ColorPickButton.Click += new System.EventHandler(this.ColorPickButton_Click);
            // 
            // teamScoreInput
            // 
            this.teamScoreInput.Location = new System.Drawing.Point(88, 116);
            this.teamScoreInput.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.teamScoreInput.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.teamScoreInput.Name = "teamScoreInput";
            this.teamScoreInput.Size = new System.Drawing.Size(100, 20);
            this.teamScoreInput.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Score";
            // 
            // teamLightInput
            // 
            this.teamLightInput.Location = new System.Drawing.Point(88, 90);
            this.teamLightInput.Name = "teamLightInput";
            this.teamLightInput.Size = new System.Drawing.Size(100, 20);
            this.teamLightInput.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Light #";
            // 
            // TeamForm
            // 
            this.AcceptButton = this.TeamSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TeamCancelAdd;
            this.ClientSize = new System.Drawing.Size(209, 211);
            this.Controls.Add(this.teamLightInput);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.teamScoreInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ColorPickButton);
            this.Controls.Add(this.teamColorInput);
            this.Controls.Add(this.teamSoundInput);
            this.Controls.Add(this.teamButtonInput);
            this.Controls.Add(this.TeamCancelAdd);
            this.Controls.Add(this.TeamSubmit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.teamNameInput);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "TeamForm";
            this.Text = "Add Team";
            ((System.ComponentModel.ISupportInitialize)(this.teamButtonInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamScoreInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamLightInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox teamNameInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button TeamSubmit;
        private System.Windows.Forms.Button TeamCancelAdd;
        private System.Windows.Forms.NumericUpDown teamButtonInput;
        private System.Windows.Forms.ComboBox teamSoundInput;
        private System.Windows.Forms.MaskedTextBox teamColorInput;
        private System.Windows.Forms.ColorDialog ColorPicker;
        private System.Windows.Forms.Button ColorPickButton;
        private System.Windows.Forms.NumericUpDown teamScoreInput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown teamLightInput;
        private System.Windows.Forms.Label label6;
    }
}