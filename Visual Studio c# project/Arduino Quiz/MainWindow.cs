﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace Arduino_Quiz
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Path.Combine(Application.StartupPath, Program.settingsIni.IniReadValue("Images", "Directory"), Program.settingsIni.IniReadValue("Images", "Icon")));

            InitializeComponent();

            this.ComPort.AutoCompleteCustomSource.AddRange(SerialPort.GetPortNames());
            this.ComPort.DataSource = SerialPort.GetPortNames();

            Program.EnableLights = this.ShowLights.Checked;
            Program.PlaySounds = this.PlaySound.Checked;
        }

        private void Settings_FormClosing(System.Object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            Program.SetStripColor("#000000");

            Thread.Sleep(100);

            Program.StopSerial();
        }

        private void SendByte_Click(object sender, EventArgs e)
        {
            Program.SerialSend(this.ByteInput.Text);
        }

        private void StartSerial_Click(object sender, EventArgs e)
        {
            Program.StartSerial(this.ComPort.Text);
        }

        private void StopSerial_Click(object sender, EventArgs e)
        {
            Program.StopSerial();
        }

        private void ShowLights_CheckedChanged(object sender, EventArgs e)
        {
            Program.EnableLights = this.ShowLights.Checked;
        }

        private void PlaySound_CheckedChanged(object sender, EventArgs e)
        {
            Program.PlaySounds = this.PlaySound.Checked;
        }

        private void AddTeam_Click(object sender, EventArgs e)
        {
            TeamForm form = new TeamForm();
            form.TeamButtonInput = TeamsList.Items.Count;
            form.TeamLightInput = TeamsList.Items.Count;
            form.ShowDialog();

            UpdateTeamList();

            TeamsList.SelectedIndex = TeamsList.Items.Count - 1;
        }

        private void EditTeam_Click(object sender, EventArgs e)
        {
            if (TeamsList.SelectedItem == null)
                return;

            string a = (string)TeamsList.SelectedItem;

            foreach (Team t in Program.Teams)
            {
                if (t.Name == a)
                {
                    TeamForm form = new TeamForm();

                    form.TeamNameInput = t.Name;
                    form.OldTeamName = t.Name;
                    form.TeamSoundInput = t.Sound;
                    form.TeamButtonInput = t.Button;
                    form.TeamLightInput = t.Light;
                    form.TeamScoreInput = t.Score;
                    form.TeamColorInput = t.HexColor;
                    form.TeamSubmitButton = "Edit";
                    form.TeamFormName = "Edit Team";

                    form.ShowDialog();

                    UpdateTeamList();
                }
            }

            TeamsList.SelectedItem = a;
        }

        private void DelTeam_Click(object sender, EventArgs e)
        {
            if (TeamsList.SelectedItem == null)
                return;

            string a = (string)TeamsList.SelectedItem;

            foreach (Team t in Program.Teams)
            {
                if (t.Name == a)
                {
                    Program.Teams.Remove(t);
                    break;
                }
            }

            UpdateTeamList();
            try
            {
                TeamsList.SelectedIndex = 0;
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void AddScoreButton_Click(object sender, EventArgs e)
        {
            if (TeamsList.SelectedItem == null)
                return;

            string a = (string)TeamsList.SelectedItem;

            foreach (Team t in Program.Teams)
            {
                if (t.Name == a)
                {
                    t.Score += (int)AddScoreInput.Value;

                    if (Program.FullScreenForm != null)
                        Program.FullScreenForm.RefreshScreen();

                    break;
                }
            }

            if (Program.FullScreenForm != null)
                Program.FullScreenForm.RefreshScreen();
        }

        private void FullscreenWindow_Click(object sender, EventArgs e)
        {
            if (Program.FullScreenForm == null)
                Program.FullScreenForm = new FullScreen();

            if (Program.GetSecondaryScreen() == null && !Program.FullScreenForm.Visible)
            {
                if (MessageBox.Show("There isn't a second screen detected, do you want to use your primary screen?", "No Secondary Monitor", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }

            if (Program.FullScreenForm.Visible)
                Program.FullScreenForm.Hide();
            else
                Program.FullScreenForm.Show();
        }

        private void UpdateTeamList()
        {
            TeamsList.Items.Clear();

            List<string> teamnames = new List<string>();
            foreach (Team t in Program.Teams)
            {
                teamnames.Add(t.Name);
            }

            TeamsList.Items.AddRange(teamnames.ToArray());

            if (Program.FullScreenForm != null)
                Program.FullScreenForm.RefreshScreen();
        }

        private void ResetTeams_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to reset everything?", "Reset Teams", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Program.Teams.Clear();
                UpdateTeamList();
            }
        }

        private void MoveItemUp_Click(object sender, EventArgs e)
        {
            if (TeamsList.SelectedItem == null)
                return;

            string a = (string)TeamsList.SelectedItem;
            int i = TeamsList.SelectedIndex;

            foreach (Team t in Program.Teams)
            {
                if (t.Name == a)
                {
                    Program.Teams.Remove(t);
                    Program.Teams.Insert((TeamsList.SelectedIndex - 1 < 0 ? 0 : TeamsList.SelectedIndex - 1), t);
                    break;
                }
            }

            UpdateTeamList();
            TeamsList.SetSelected((i - 1 < 0 ? 0 : i - 1), true);
        }

        private void MoveItemDown_Click(object sender, EventArgs e)
        {
            if (TeamsList.SelectedItem == null)
                return;

            string a = (string)TeamsList.SelectedItem;
            int i = TeamsList.SelectedIndex;

            foreach (Team t in Program.Teams)
            {
                if (t.Name == a)
                {
                    Program.Teams.Remove(t);
                    Program.Teams.Insert((TeamsList.SelectedIndex + 1 >= Program.Teams.Count ? Program.Teams.Count : TeamsList.SelectedIndex + 1), t);
                    break;
                }
            }

            UpdateTeamList();
            TeamsList.SetSelected((i + 1 >= Program.Teams.Count ? Program.Teams.Count - 1 : i + 1), true);
        }

        private void OpenSoundboard_Click(object sender, EventArgs e)
        {
            SoundboardForm soundboard = new SoundboardForm();
            soundboard.Show();
        }

        private void OpenTimer_Click(object sender, EventArgs e)
        {
            TimerForm timer = new TimerForm();
            timer.Show();
        }

        private void OpenLedStripForm_Click(object sender, EventArgs e)
        {
            LedStripForm form = new LedStripForm();
            form.Show();
        }

        private void AutomaticTimer_Click(object sender, EventArgs e)
        {
            Program.AutomaticTimerForm = new TimerForm();
            Program.AutomaticTimerForm.Text = "Automatic Timer";
            Program.AutomaticTimerForm.TimerStart.Hide();
            Program.AutomaticTimerForm.Show();
            MessageBox.Show("Keep this window open to use the automatic timer");
        }
    }
}
